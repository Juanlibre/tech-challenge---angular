import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IWords } from './../../interfaces/api.interface';
import { ApiService } from './../../services/api.service';

@Component({
  selector: 'app-new-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.scss']
})
export class NewFormComponent implements OnInit {
  constructor(private apiService: ApiService, private router: Router) {}

  ngOnInit() {}

  // Create the word and navigate home after it resolves
  onFormSubmit(word: IWords) {
    this.apiService.setCreateWord(word)
      .then(() => {
        this.router.navigate(['/']);
      })
  }
}
